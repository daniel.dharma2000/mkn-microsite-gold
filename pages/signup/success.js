import React from "react"
import Link from 'next/link'
import Image from 'next/image'

export default function SignupSuccess() {
    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center">
                    <Link href="/">
                        <a>
                            <Image src="/img/back.webp" 
                                height="24"
                                width="24"
                                alt="back" />
                        </a>
                    </Link>
                    <div className="text-center">
                        <Image src="/img/logo_nyata.webp"
                            height="24"
                            width="36"
                            alt="Nyata" />
                    </div>
                </div>
            </div>
            
            <div className="sm:container mx-auto p-4 custom-pt-70">
                <div className="mt-20">
                    <div className="text-center">
                        <Image src="/img/check.png" 
                            height="128"
                            width="128"
                            alt="Success" />
                    </div>
                    <div className="mt-5 text-center heading5">
                        Registrasi Sukses
                    </div>
                    <div className="mt-10 text-center">
                        Registrasi akun anda telah berhasil<br/>
                        Silahkan gunakan email dan password anda untuk login
                    </div>
                    <div className="mt-10">
                        <Link href="/login">
                            <a className="button-primary block">
                                Login
                            </a>
                        </Link>
                    </div>
                </div>  
            </div>
        </>
    )
}
