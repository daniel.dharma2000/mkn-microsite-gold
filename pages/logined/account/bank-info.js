import React, {useState, useEffect} from "react"
import Link from 'next/link'
import Image from 'next/image'
import { useRouter } from 'next/router'
import * as API from "services/api"
import LoadingPage from "components/loadingPage"
import EmptyBank from "components/bank-info/empty-bank"
import PendingBank from "components/bank-info/pending-bank"

export default function BankInfo() {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loadingPage: true,
            customer_bank:{},
            bank:[]
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getMerchantCustomerBank = async() => {
        setLocalState(initialLocalState);
        try {
            var response = await API.Get('/customer_bank', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {            
            router.back();
            return true;
        }

        if(response.code == "000") {
            let objLocalState = localState;
            objLocalState.customer_bank = response.data.customer_bank;
            objLocalState.loadingPage = false;
            setLocalState({...objLocalState, ...localState});
            return true;
        }
        else {
            router.back();
            return true;
        }
    }
    useEffect(() => {
        getMerchantCustomerBank();
    }, []);

    return (
        <>
            {localState.loadingPage
                ?
                <LoadingPage />
                :
                <div className="sm:container mx-auto p-4 custom-pb-86">
                    <div className="grid grid-cols-3 items-center">
                        <div>
                            <Link href="/logined/account">
                                <a>
                                    <Image src="/img/back.png" 
                                        height="12"
                                        width="6"
                                        alt="back" />
                                </a>
                            </Link>
                        </div>

                        <div className="font-epilogue font-bold text-xl text-center">
                            Bank
                        </div>
                    </div>

                    {!localState.customer_bank.status
                        ?
                        <EmptyBank />
                        :
                        <>
                        {localState.customer_bank.status == "pending" 
                            ?
                            <PendingBank customerBank={localState.customer_bank} />
                            :
                            <></>
                        }
                        </>
                    }
                </div>
            }
        </>
    )
}
