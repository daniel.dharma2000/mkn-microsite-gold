import React, {useState, useEffect} from "react"
import Link from 'next/link'
import Image from 'next/image'
import { useRouter } from 'next/router'
import TotalGold from "components/gold/totalGold"

export default function Sell() {
    const router = useRouter()
    return (
        <>
            <div className="sm:container mx-auto p-4 custom-pb-86">
                <div className="grid grid-cols-3 items-center">
                    <div>
                        <Link href="/logined/gold">
                            <a>
                                <Image src="/img/back.png" 
                                    height="12"
                                    width="6"
                                    alt="back" />
                            </a>
                        </Link>
                    </div>

                    <div className="font-epilogue font-bold text-xl text-center">
                        Sell Gold
                    </div>
                </div>

                <div className="mt-10">
                    <TotalGold />
                </div>

                <div className="mt-5 text-center">
                    <div className="text-sm">Sell Price</div>
                    <div className="font-epilogue font-bold text-xl">Rp. 800.000/gr</div>
                </div>

                <div className="mt-5">
                    <label className="text-xs">Sell Amount</label>
                    <div className="flex">
                        <div className="mr-3 text-2xl">Rp </div>
                        <input type="tel" 
                            className="border-b w-full text-2xl"
                            placeholder="Sell Amount"
                            value="0" />
                    </div>
                </div>
                <div className="text-xs mt-2 color-red">Minimum sell is Rp. 10.000</div>

                <hr className="mt-5" />

                <div className="mt-5 bg-gray-2 rounded-xl px-5 py-3">
                    <label className="text-xs">Weight</label>
                    <div className="mr-3 text-2xl">1 gr</div>
                </div>

                <div className="mt-10">
                    <Link href="sell/confirm">
                        <a className="button-primary rounded-lg px-5 py-3 font-medium w-full block text-center">
                            Next
                        </a>
                    </Link>
                </div>
            </div>
        </>
    )
}
