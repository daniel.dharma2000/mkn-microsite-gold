import React from "react";
import BuyInquiry from "components/gold/transaction/buy/inquiry";
import BuyCheckout from "components/gold/transaction/buy/checkout";
import GoldBuyProvider from "providers/goldBuyProvider";

export default function Buy() {
    return (
        <GoldBuyProvider>
            <BuyInquiry />
            <BuyCheckout />
        </GoldBuyProvider>
    )
}
