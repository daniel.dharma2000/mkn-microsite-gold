import React, {useState, useEffect} from "react"
import Image from 'next/image'
import Link from 'next/link'
import * as API from "services/api";
import * as errorCode from "services/error_codes";
import { useRouter } from 'next/router'
import LoadingPage from "components/loadingPage";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function BuyDetail() {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loadingPage:true,
            loading:false,
            isError:false,
            error_message:"",
            transaction:null
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getTransaction = async () => {
        setLocalState(initialLocalState());

        try {
            var response = await API.Get('/account/gold/buy/detail', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}&transaction_code=${router.query.code}`
            );
        }
        catch(error) {    
            router.reload();
            return true;
        }

        if(response.code == "000") {
            let objLocalState = localState;
            objLocalState.loadingPage = false;
            objLocalState.transaction = response.data.buy;
            setLocalState({...objLocalState, ...localState}); 
        }
        else {
            router.back();
            return true;
        }
        return true;
    }
    const copyVA = () => {
        navigator.clipboard.writeText(localState.transaction.active_v_a_xendit.virtual_account);
        toast("Virtual Akun berhasil disalin");
    }
    useEffect(() => {
        getTransaction();        
    }, []);

    return (
        <>
            {localState.loadingPage
                ?
                <LoadingPage />
                :
                <div className="sm:container mx-auto p-4 custom-pb-86">
                    <ToastContainer />
                    <div className="grid grid-cols-3 items-center">
                        <div>
                            <button type="button" 
                                onClick={() => router.back()}>
                                <Image src="/img/back.png" 
                                    height="12"
                                    width="6"
                                    alt="back" />
                            </button>
                        </div>
                    
                        <div className="font-epilogue font-bold text-xl text-center">
                            Beli Emas
                        </div>
                    </div>

                    <div className="mt-10 grid grid-cols-2 items-center">
                        <label>Kode Transaksi</label>
                        <div className="text-right text-xs">{router.query.code}</div>
                    </div>

                    <div className="mt-5 grid grid-cols-2">
                        <label>Harga Beli</label>
                        <div className="text-right text-xs">IDR {(localState.transaction.price).toLocaleString()}/gr</div>
                    </div>

                    <div className="mt-5 grid grid-cols-2">
                        <label>Jumlah</label>
                        <div className="text-right text-xs">{localState.transaction.credit} gr</div>
                    </div>

                    <div className="mt-5 grid grid-cols-2">
                        <label>Nilai Beli</label>
                        <div className="text-right text-xs">IDR {(localState.transaction.subtotal).toLocaleString()}</div>
                    </div>

                    <div className="mt-5 grid grid-cols-2">
                        <label>Biaya Layanan</label>
                        <div className="text-right text-xs">IDR {(localState.transaction.service_fee).toLocaleString()}</div>
                    </div>

                    <hr className="mt-5" />

                    <div className="mt-5 grid grid-cols-2">
                        <label>Total</label>
                        <div className="text-right font-medium">IDR {(localState.transaction.grandtotal).toLocaleString()}</div>
                    </div>

                    <div className="mt-5 grid grid-cols-2">
                        <label>Metode Pembayaran</label>
                        <div className="text-right font-medium">{localState.transaction.payment_method.text}</div>
                    </div>

                    <div className="mt-5 grid grid-cols-2">
                        <label>Virtual Akun</label>
                        <div className="text-right font-medium">
                            <span>{localState.transaction.active_v_a_xendit.virtual_account}</span>
                            <span className="ml-2">
                                <button onClick={() => copyVA()}>
                                    <Image src="/img/ico-copy.png" 
                                        height="16"
                                        width="16"
                                        alt="copy"
                                        title="Copy Virtual Account" />
                                </button>
                            </span>
                        </div>
                    </div>

                    <div className="mt-10 text-center text-sm">
                        <div>Harap selesaikan pembayaran sebelum</div>
                        <div>{localState.transaction.expired_at}</div>
                    </div>

                    <div className="mt-10">
                        <Link href="../">
                            <a className="button-primary rounded-lg px-5 py-3 font-medium w-full block text-center">
                                Kembali
                            </a>
                        </Link>
                    </div>
                </div>
            }
        </>
    )
}
