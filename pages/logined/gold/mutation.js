import React, {useState, useEffect} from "react";
import Link from 'next/link';
import Image from 'next/image';
import { useRouter } from 'next/router';
import * as API from "services/api";
import LoadingPage from "components/loadingPage";

export default function GoldMutation() {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loadingPage: true, 
            range:"1month",
            transactions:[]
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getMutation = async() => {
        let objLocalState = localState;
        objLocalState.loadingPage = true;
        objLocalState.transactions = [];
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/account/gold/mutation', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}&range=${localState.range}`);
        }
        catch(error) {            
            router.back();
            return true;
        }

        if(response.code == "000") {
            objLocalState.transactions = response.data.transactions;
            objLocalState.loadingPage = false;
            setLocalState({...objLocalState, ...localState});
            return true;
        }
        else {
            router.back();
            return true;
        }
    }
    const getUser = async() => {
        let objLocalState = localState;
        objLocalState.customer = {};
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/customer', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            localStorage.removeItem("logined");
            localStorage.removeItem("logined_user_reference");
            localStorage.removeItem("logined_user_fullname");
            localStorage.removeItem("logined_token");
            router.push('/');
        }

        if(response.code == "000") {
            objLocalState.customer = response.data.customer;
            setLocalState({...objLocalState, ...localState});            
        }     
        else {
            localStorage.removeItem("logined");
            localStorage.removeItem("logined_user_reference");
            localStorage.removeItem("logined_user_fullname");
            localStorage.removeItem("logined_token");
            router.push('/');
        }
        return true;
    }
    const changeRange = (e) => {
        let objLocalState = localState;
        objLocalState.range = e.target.value;
        setLocalState({...objLocalState, ...localState});
        getMutation();
    }
    useEffect(() => {
        getUser();
        getMutation();
    }, []);

    return (
        <>
            {localState.loadingPage
                ?
                <LoadingPage />
                :
                <div className="sm:container mx-auto p-4 custom-pb-86">
                    <div className="grid grid-cols-3 items-center">                    
                        <Link href="/logined/gold">
                            <a>
                                <Image src="/img/back.png" 
                                    height="12"
                                    width="6"
                                    alt="back" />
                            </a>
                        </Link>

                        <div className="font-epilogue font-bold text-xl text-center">
                            Mutasi
                        </div>
                    </div>

                    <div className="mt-10">
                        <select className="w-full"
                            value={localState.range}
                            onChange={(e)=>changeRange(e)}>
                            <option value="1month">1 Bulan</option>
                            <option value="2months">2 Bulan</option>
                            <option value="6months">6 Bulan</option>
                        </select>
                    </div>

                    <div className="mt-3 divide-y divide-slate-200">
                        {localState.transactions.map((item,index) => {
                            return (
                                <div className="py-3"
                                    key={index}>
                                    <div className={"font-epilogue font-bold text-black text-md " + (item.debet ? "color-red" : "color-green")}>
                                        {item.debet || item.credit} gr
                                    </div>
                                    <div className="text-xs color-gray">{item.code}</div>
                                    <div className="grid grid-cols-2 text-xs color-gray">
                                        <div>
                                            <span>{item.debet ? "Jual" : "Beli"} </span>
                                            <span>IDR {(parseInt(item.grandtotal)).toLocaleString()}</span>
                                        </div>
                                        <div className="text-right">
                                            {item.activate_at ? item.activate_at : "PEND"}
                                        </div>
                                    </div>
                                </div> 
                            )
                        })}
                    </div>
                </div>
            }
        </>
    )
}
