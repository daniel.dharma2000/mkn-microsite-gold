import React from "react"
import Link from 'next/link'
import Image from 'next/image'
import { useRouter } from 'next/router'

export default function TransactionBuy() {
    const router = useRouter()
    return (
        <>
            <div className="sm:container mx-auto p-4 custom-pb-86">
                <div className="grid grid-cols-3 items-center">
                    <div>
                        <Link href="/logined/transaction">
                            <a>
                                <Image src="/img/back.png" 
                                    height="12"
                                    width="6"
                                    alt="back" />
                            </a>
                        </Link>
                    </div>

                    <div className="font-epilogue font-bold text-xl text-center">
                        1234567
                    </div>
                </div>

                <div className="mt-7 grid grid-cols-2">
                    <label>Weight</label>
                    <div className="text-right">0.1 gr</div>
                </div>

                <div className="mt-5 grid grid-cols-2">
                    <label>Unit Price</label>
                    <div className="text-right">Rp. 800.000/gr</div>
                </div>

                <div className="mt-5 grid grid-cols-2">
                    <label>Purchase Amount</label>
                    <div className="text-right">Rp. 80.000</div>
                </div>

                <div className="mt-5 grid grid-cols-2">
                    <label>Service Fee</label>
                    <div className="text-right">Rp.5.000</div>
                </div>

                <hr className="mt-5" />

                <div className="mt-5 grid grid-cols-2">
                    <label>Total Payment</label>
                    <div className="text-right font-medium">Rp.85.000</div>
                </div>

                <div className="mt-5 grid grid-cols-2">
                    <label>Bank</label>
                    <div className="text-right font-medium">BCA</div>
                </div>

                <div className="mt-5 grid grid-cols-2">
                    <label>Virtual Account</label>
                    <div className="text-right font-medium">123456789</div>
                </div>

                <div className="mt-10">
                    <button className="button-primary rounded-lg px-5 py-3 font-medium w-full">
                        Check Payment
                    </button>
                </div>

                <div className="mt-5">
                    <button className="button bg-red text-white rounded-lg px-5 py-3 font-medium w-full">
                        Cancel Transaction
                    </button>
                </div>

                <div className="mt-10 text-center text-sm color-red">
                    <div>Please proceed the transaction before</div>
                    <div>24 June 2022 23:59</div>
                </div>
            </div>
        </>
    )
}
