import React from "react"
import Link from 'next/link'
import Image from 'next/image'
import TotalAssets from "components/dashboard/totalAssets"
import FooterPage from "components/footerPage"

export default function Transaction() {
    return (
        <>
            <div className="sm:container mx-auto p-4 custom-pb-86">
                <div className="grid grid-cols-3 items-center">
                    <div>
                        <Link href="/logined/dashboard">
                            <a>
                                <Image src="/img/back.png" 
                                    height="12"
                                    width="6"
                                    alt="back" />
                            </a>
                        </Link>
                    </div>

                    <div className="font-epilogue font-bold text-xl text-center">
                        Transaction
                    </div>
                </div>

                <div className="mt-7 mb-5">
                    <TotalAssets />
                </div>

                <Link href="transaction/buy?code=123">
                    <a className="py-3 border-b block">                                            
                        <div className="font-epilogue font-bold text-black text-md color-green">0.05 gr</div>
                        <div className="grid grid-cols-2 text-xs color-gray">
                            <div>Buy Rp. 200.000</div>
                            <div className="text-right">22 Jun 2022</div>
                        </div>
                        <div className="label-pending rounded-full bg-gray inline text-xs px-2 text-white">Pending</div>                    
                    </a>
                </Link>

                <Link href="transaction/sell?code=123">
                    <a className="py-3 border-b block">                    
                        <div className="font-epilogue font-bold text-black text-md color-red">0.05 gr</div>
                        <div className="grid grid-cols-2 text-xs color-gray">
                            <div>Sell Rp. 200.000</div>
                            <div className="text-right">22 Jun 2022</div>
                        </div>
                        <div className="label-pending rounded-full bg-gray inline text-xs px-2 text-white">Pending</div>                    
                    </a>
                </Link>

                <div className="py-3 border-b">
                    <div className="font-epilogue font-bold text-black text-md color-green">0.05 gr</div>
                    <div className="grid grid-cols-2 text-xs color-gray">
                        <div>Buy Rp. 200.000</div>
                        <div className="text-right">22 Jun 2022</div>
                    </div>
                </div>

                <div className="py-3 border-b">
                    <div className="font-epilogue font-bold text-black text-md color-green">0.05 gr</div>
                    <div className="grid grid-cols-2 text-xs color-gray">
                        <div>Buy Rp. 200.000</div>
                        <div className="text-right">22 Jun 2022</div>
                    </div>
                </div>

                <div className="py-3 border-b">
                    <div className="font-dmsans font-bold text-black text-md color-red">0.05 gr</div>
                    <div className="grid grid-cols-2 text-xs color-gray">
                        <div>Sell Rp. 200.000</div>
                        <div className="text-right">22 Jun 2022</div>
                    </div>
                </div>

                <div className="py-3 border-b">
                    <div className="font-epilogue font-bold text-black text-md color-green">0.05 gr</div>
                    <div className="grid grid-cols-2 text-xs color-gray">
                        <div>Buy Rp. 200.000</div>
                        <div className="text-right">22 Jun 2022</div>
                    </div>
                </div>
            </div>

            <FooterPage selected="transaction" />
        </>
    )
}
