import React, {useState, useEffect} from "react";
import Link from 'next/link';
import Image from 'next/image';
import { useRouter } from 'next/router';
import LoadingPage from "components/loadingPage";
import * as API from "services/api";

export default function BankAccountActivation() {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loadingPage: true, 
            loadingProcess: false,
            account: {},
            banks: [],
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getUser = async() => {
        let objLocalState = localState;
        objLocalState.account = {};
        objLocalState.loadingPage = true;
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/customer', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            router.push('/logined/dashboard');
            return true;
        }

        if(response.code == "000") {
            objLocalState.loadingPage = false;
            objLocalState.account.fullname = response.data.customer.fullname;
            objLocalState.account.email = response.data.customer.email;
            objLocalState.account.dob = response.data.customer.birthdate;
            objLocalState.account.gender = response.data.customer.gender;
            setLocalState({...objLocalState, ...localState});            
        }     
        else router.push('/logined/dashboard');
        return true;
    }
    const getBanksCode = async() => {
        try {
            var response = await API.Get('/straits/bank', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}`
            );
        }
        catch(error) {
            return true;
        }

        if(response.code == "000") {
            let objLocalState = localState;            
            objLocalState.banks = response.data.banks;
            setLocalState({...objLocalState, ...localState}); 
        }
    }
    const saveAccount = async(e) => {
        e.preventDefault();

        let objLocalState = localState;
        objLocalState.loadingProcess = true;
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Post('/account/straits/update_register', {
                code: localStorage.getItem("merchantcode"),
                client_code: localStorage.getItem("merchantClientcode"),
                token: localStorage.getItem("logined_token"),
                bank_account: localState.account.bank_account,
                bank_owner: localState.account.bank_owner,
                bank: localState.account.bank
            });
        }
        catch(error) {
            router.push('/logined/dashboard');
            return true;
        }

        if(response.code == "000") {
            router.push("/logined/index/account/success");
            return true;
        }
        objLocalState.loadingProcess = false;
        setLocalState({...objLocalState, ...localState});
    }
    useEffect(() => {
        getUser();
        getBanksCode();
    }, []);

    return (
        <>
            {localState.loadingPage
                ?
                <LoadingPage />
                :
                <>
                    <div className="fixed bg-white w-full z-10">
                        <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center">
                            <div className="text-left">
                                <Link href="/logined/index">
                                    <a>
                                        <Image src="/img/back.webp" 
                                            height="24"
                                            width="24"
                                            alt="back" />
                                    </a>
                                </Link>
                            </div>
                            <div className="text-center subtitle1">
                                Aktivasi Bank
                            </div>                    
                        </div>
                    </div>

                    <div className="sm:container mx-auto p-4 custom-pt-70">                
                        <form onSubmit={(e)=>saveAccount(e)}>
                            <div className="border rounded py-1 px-3">
                                <label>Nomor Rekening Bank</label>
                                <input type="tel"
                                    placeholder="Nomor Rekening Bank"
                                    className="w-full"
                                    value={localState.account.bank_account ?? ""}
                                    onInput={(e) => setLocalState({...localState, account:{...localState.account, bank_account:e.target.value}})}
                                    required={true} />
                            </div>

                            <div className="mt-2 border rounded py-1 px-3">
                                <label>Nama Pemilik Rekening</label>
                                <input type="text"
                                    placeholder="Nama Pemilik Rekening Bank"
                                    className="w-full"
                                    value={localState.account.bank_owner ?? ""}
                                    onInput={(e) => setLocalState({...localState, account:{...localState.account, bank_owner:e.target.value}})}
                                    required={true} />
                            </div>

                            <div className="mt-2 border rounded py-1 px-3">
                                <label>Nama Bank</label>
                                <select className="w-full bg-transparent"
                                    value={localState.account.bank ?? ""}
                                    onInput={(e) => setLocalState({...localState, account:{...localState.account, bank:e.target.value}})}
                                    required={true}>
                                    <option value="">Pilih</option>
                                    {localState.banks.map((item,index) => {
                                        return (
                                            <option key={index}
                                                value={item.code}>{item.name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            
                            <div className="mt-5">
                                <button className="button-primary w-full"
                                    type="submit">
                                    Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                </>
            }
        </>
    )
}
