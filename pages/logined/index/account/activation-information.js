import React from "react";
import Link from 'next/link'
import Image from 'next/image';
import { useRouter } from 'next/router';

export default function AccountInformation() {
    const router = useRouter();
    return (
        <>
            <div className="sm:container p-4 fixed bg-white w-full z-10">                       
                <div className="grid grid-cols-3 items-center">  
                    <div className="text-left">
                        <Link href="/logined/index">
                            <a>
                                <Image src="/img/back.webp" 
                                    height="24"
                                    width="24"
                                    alt="back" />
                            </a>
                        </Link>
                    </div>
                    <div className="text-center subtitle1">
                        Aktivasi
                    </div>
                </div>
            </div>

            <div className="sm:container mx-auto p-4 custom-pt-70">                            
                <div className="text-center mt-5">
                    <Image src="/img/index.png" 
                        height="64"
                        width="64"
                        alt="Kayya" />
                </div>
                <div className="text-justify mt-10">
                    Kayya difasilitasi oleh PT Straits Futures Indonesia, Pialang Berjangka yang diawasi oleh Bappebti berdasarkan izin nomor 43/BAPPEBTI/SI/09/2015. Transaksi Anda akan diregistrasikan ke Jakarta Futures Exchange dan dijamin penyelesaiannya oleh PT Kliring Berjangka Indonesia (Persero)
                </div>   
                <div className="mt-10">
                    <Link href="/logined/index/account/activation">
                        <a className="button-primary block">
                            Lanjutkan
                        </a>
                    </Link>
                </div>
            </div>
        </>
    )
}
