import React from "react";
import Link from 'next/link'
import Image from 'next/image';
import { useRouter } from 'next/router';

export default function TransactionSuccess() {
    const router = useRouter();
    return (
        <>
            <div className="sm:container p-4 fixed bg-white w-full z-10">                       
                <div className="grid grid-cols-3 items-center">  
                    <div className="text-left">
                        <Link href="/logined/index">
                            <a>
                                <Image src="/img/back.webp" 
                                    height="24"
                                    width="24"
                                    alt="back" />
                            </a>
                        </Link>
                    </div>
                    <div className="text-center subtitle1">
                        Transaksi
                    </div>
                </div>
            </div>

            <div className="sm:container mx-auto p-4 custom-pt-70">            
                <div className="mt-20">
                    <div className="text-center">
                        <Image src="/img/error.png" 
                            height="128"
                            width="128"
                            alt="Error" />
                    </div>
                    <div className="mt-5 text-center color-green heading5">
                        Gagal
                    </div>
                    <div className="mt-10 text-center">
                        Maaf, Permintaan transaksi anda mengalami kegagalan,<br/>
                        Harap lakukan transaksi kembali
                    </div>
                    <div className="mt-10">
                        <Link href="/logined/index">
                            <a className="button-primary block">
                                Kembali
                            </a>
                        </Link>
                    </div>
                </div>      
            </div>
        </>
    )
}