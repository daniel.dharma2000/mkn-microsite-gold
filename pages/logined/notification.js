import React, {useState, useEffect} from "react";
import Link from 'next/link';
import Image from 'next/image';
import { useRouter } from "next/router";
import * as API from "services/api";
import LoadingPage from "components/loadingPage";
import HeaderPage from "components/headerPage";
import News from "components/dashboard/news";
import Promo from "components/dashboard/promos";
import FooterPage from "components/footerPage";

export default function Notification() {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loadingPage: true,
            notifications: [],
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getNotifications = async() => {
        setLocalState(initialLocalState);
        try {
            var response = await API.Get('/notification', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            router.push('/logined/dashboard');
            return true;
        }

        if(response.code == "000") {
            let objLocalState = localState;
            objLocalState.notifications = response.data.notifications;
            objLocalState.loadingPage = false;
            setLocalState({...objLocalState, ...localState});
        }
        else router.push('/logined/dashboard');
        return true;
    }
    useEffect(() => {
        getNotifications();
    }, []);

    return (
        <>
            {localState.loadingPage
                ?
                <LoadingPage />
                :
                <>
                    <div className="sm:container p-4 fixed bg-white w-full z-10">                       
                        <div className="grid grid-cols-3 items-center">  
                            <div className="text-left">
                                <button type="button" 
                                    onClick={() => router.back()}>
                                    <Image src="/img/back.webp" 
                                        height="24"
                                        width="24"
                                        alt="back" />
                                </button>
                            </div>
                            <div className="text-center subtitle1">
                                Notification
                            </div>
                        </div>
                    </div>

                    <div className="sm:container mx-auto custom-pt-70">
                        {localState.notifications.length > 0 
                            ?
                            <div className="divide-y">
                                {localState.notifications.map((item, index) => {
                                    return (
                                        <div key={index}
                                            className={"py-3 px-4 " + (item.isread == "0" && "bg-gray-2")}>
                                            <div className="subtitle2">{item.title}</div>
                                            <div>{new Date(item.created_at).toLocaleString()}</div>
                                            <div className="mt-1">{item.content}</div>
                                        </div>
                                    )
                                })}
                            </div>
                            :
                            <div className="text-center mt-10">
                                Tidak ada notifikasi baru
                            </div>
                        }
                    </div>
                </>
            }
        </>
    )
}
