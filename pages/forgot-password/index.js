import React, {useState} from "react"
import { useRouter } from 'next/router'
import Link from 'next/link'
import Image from 'next/image'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleExclamation } from '@fortawesome/free-solid-svg-icons'
import * as API from "services/api"
import * as errorCode from "services/error_codes"

export default function ForgotPassword() {
    const router = useRouter();
    const initialLocalState=()=>{
        return {
            loading:false, isError:false, error_message:"",
            input_email:"",
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const forgotPassword = async(e) => {        
        e.preventDefault();
        let objLocalState = localState;
        objLocalState.loading = true;
        setLocalState({...objLocalState, ...localState});
        
        try {
            var response = await API.Post('/auth/forgot_password', {
                code : localStorage.getItem("merchantcode"),
                client_code : localStorage.getItem("merchantClientcode"),
                email : localState.input_email,
            });
        }
        catch(error) {                       
            objLocalState.loading = false;
            objLocalState.isError = true;
            objLocalState.error_message = error;
            setLocalState({...objLocalState, ...localState});
            return true;
        }

        if(response.code == "000") {
            objLocalState.loading = false;
            objLocalState.isError = false;
            setLocalState({...objLocalState, ...localState});
            router.push(`forgot-password/otp?token=${response.data.token}`, undefined, { shallow: true });
        }
        else {
            objLocalState.loading = false;
            objLocalState.isError = true;
            objLocalState.error_message = response.message ? response.message : errorCode.getErrorCodes(response.code);
            setLocalState({...objLocalState, ...localState});
        }
    }

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center">        
                    <Link href="/login">
                        <a>
                            <Image src="/img/back.webp" 
                                height="24"
                                width="24"
                                alt="back" />
                        </a>
                    </Link>
                    <div className="text-center">
                        <Image src={process.env.NEXT_PUBLIC_LOGO}
                            height="24"
                            width="36"
                            alt="Nyata" />
                    </div>
                </div>
            </div>

            <div className="sm:container mx-auto p-4 custom-pt-70">
                <div className="text-center heading5">
                    Lupa Password
                </div>
                <div className="text-center subtitle1">
                    Masukan email anda untuk melanjutkan proses lupa password
                </div>
                <form onSubmit={(e)=>forgotPassword(e)}>
                    <div className="mt-20">
                        <input type="email" 
                            placeholder="Email"
                            className="border rounded-lg p-3 w-full"
                            onInput={(e)=>setLocalState({...localState, input_email:e.target.value})}
                            value={localState.input_email}
                            autoComplete="false"
                            required={true}
                            autoFocus={true} />
                    </div>
                    {localState.isError && 
                        <div className="mt-2 bg-red-7 p-3 rounded text-white">
                            <FontAwesomeIcon icon={faCircleExclamation}
                                className="mr-1" />
                            {localState.error_message}
                        </div>
                    }

                    <div className="mt-5">
                        {localState.loading
                            ?
                            <button className="button-primary w-full"
                                type="button"
                                disabled="disabled">
                                <Image src="/img/loading.png"
                                    alt="loading"
                                    height={16}
                                    width={16}
                                    className="animate-spin" />
                                <span className="ml-3">Loading ...</span>
                            </button>
                            :      
                            <button className="button-primary w-full"
                                type="submit">
                                Lupa Password
                            </button>
                        }
                    </div>    
                </form>      
                <div className="mt-3 text-center">    
                    <Link href="login">
                        <a className="button-link w-full block">
                            Login
                        </a>
                    </Link>
                </div>
            </div>
        </>
    )
}
