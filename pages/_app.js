import React from "react";
import Head from "next/head";
import "styles/globals.css";
import '@fortawesome/fontawesome-svg-core/styles.css';
import { config } from '@fortawesome/fontawesome-svg-core';

function MyApp({ Component, pageProps }) {
    const Layout = Component.layout || (({ children }) => <>{children}</>);
    config.autoAddCss = false;
    
    return (
        <React.Fragment>
            <Head>
                <meta name="viewport"
                    content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" />
                <link rel="manifest" href="/manifest.json" />
                <title>{process.env.NEXT_PUBLIC_TITLE}</title>
                <link rel="shortcut icon" href={process.env.NEXT_PUBLIC_FAVICON} />
            </Head>
            <Layout>
                <Component {...pageProps} />
            </Layout>            
        </React.Fragment>
    )
}

export default MyApp
