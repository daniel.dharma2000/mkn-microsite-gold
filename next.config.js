/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: false,
    swcMinify: true,
    images: {
        remotePatterns: [{
            protocol: 'https',
            hostname: 'mkndev.oss-ap-southeast-5.aliyuncs.com',
        }]
    },
}

const withPWA = require('next-pwa')({
    dest: 'public',
    register: true,
})
module.exports = withPWA(nextConfig);


