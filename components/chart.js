import React, { useEffect, useRef } from 'react';
import { createChart } from 'lightweight-charts';

const Chart = (props) => {
	const chartRef = useRef(null);
	useEffect(() => {
		const chart = createChart(chartRef.current, { height: 300 });
		chart.timeScale().fitContent();
		const areaSeries = chart.addAreaSeries({ lineColor: '#2962FF', topColor: '#2962FF', bottomColor: 'rgba(41, 98, 255, 0.28)' });		
		areaSeries.setData(props.data);
	}, []);

	return <div ref={chartRef} />;
}
export default Chart;