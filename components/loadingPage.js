import React from "react";
import Image from 'next/image';

export default function LoadingPage() {
    return (
        <>
            <div className='h-screen grid p-5 text-center content-center'>
                <div>
                    <Image src="/img/loading.png"
                        alt="loading"
                        height={30}
                        width={30}
                        className="animate-spin" />
                    <div className='mt-2 text-xs'>
                        Loading
                    </div>
                </div>
            </div>
        </>
    )
}