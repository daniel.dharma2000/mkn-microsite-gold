import React from "react";
import Image from 'next/image';

export default function ErrorScreen() {
    return (
        <div className="grid place-items-center h-screen">
            <div>
                Oops
            </div>
        </div>
    )
}