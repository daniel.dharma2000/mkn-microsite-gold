import React from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

export default function Promos() {
    return (
        <Carousel showThumbs={false}>
            <div>
                <img src="/img/banner-promo.jpg"
                    alt="News" />
            </div>
            <div>
                <img src="/img/banner-promo.jpg"
                    alt="News" />
            </div>
        </Carousel>
    )
}