import React from "react";

export default function TotalAssets() {
    return (
        <div className="rounded-lg bg-primary text-white p-5 font-medium-500">
            <div className="font-medium">Nilai Portfolio Kamu</div>
            <div className="text-2xl mt-1">
                IDR 10.000.000
            </div>
        </div>
    )
}