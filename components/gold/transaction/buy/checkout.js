import React, {useState, useEffect} from "react";
import Link from 'next/link';
import Image from 'next/image';
import * as API from "services/api";
import * as errorCode from "services/error_codes";
import LoadingPage from "components/loadingPage";
import { useRouter } from 'next/router';
import { useGoldBuy } from "providers/goldBuyProvider";

export default function BuyCheckout() {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loading:false,
            isError:false,
            error_message:""
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const { globalGoldBuyState, setGlobalGoldBuyState } = useGoldBuy();
    const proceed = async() => {
        let objLocalState = localState;
        objLocalState.loading = true;
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Post('/account/gold/buy/proceed', {
                code : localStorage.getItem("merchantcode"),
                client_code : localStorage.getItem("merchantClientcode"),
                token : localStorage.getItem("logined_token"),
                amount : globalGoldBuyState.amountIDR,
                payment_method: globalGoldBuyState.paymentMethod
            });
        }
        catch(error) {    
            router.reload();
            return true;
        }

        if(response.code == "000") router.push(`buy/detail?code=${response.data.buy}`);        
        else {
            objLocalState.loading = false;
            objLocalState.isError = true;            
            objLocalState.error_message = response.message ? response.message : errorCode.getErrorCodes(response.code);
            setLocalState({...objLocalState, ...localState});
        }
        return true;
    }

    return (
        <> {globalGoldBuyState.page == "checkout" &&
            <div className="sm:container mx-auto p-4 custom-pb-86">
                <div className="grid grid-cols-3 items-center">
                    <div>
                        <Link href="/logined/gold/buy">
                            <a>
                                <Image src="/img/back.png" 
                                    height="12"
                                    width="6"
                                    alt="back" />
                            </a>
                        </Link>
                    </div>
                
                    <div className="font-epilogue font-bold text-xl text-center">
                        Konfirmasi
                    </div>
                </div>

                <div className="mt-10 grid grid-cols-2">
                    <label>Berat</label>
                    <div className="text-right">{globalGoldBuyState.amountGold} gr</div>
                </div>

                <div className="mt-5 grid grid-cols-2">
                    <label>Harga Beli</label>
                    <div className="text-right">IDR {(globalGoldBuyState.price).toLocaleString()}</div>
                </div>

                <div className="mt-5 grid grid-cols-2">
                    <label>Nilai Pembelian</label>
                    <div className="text-right">IDR {(globalGoldBuyState.amountIDR).toLocaleString()}</div>
                </div>

                <div className="mt-5 grid grid-cols-2">
                    <label>Biaya Layanan</label>
                    <div className="text-right">IDR {(globalGoldBuyState.serviceFee).toLocaleString()}</div>
                </div>

                <hr className="mt-5" />

                <div className="mt-5 grid grid-cols-2">
                    <label>Harga Total</label>
                    <div className="text-right font-medium">IDR {(globalGoldBuyState.amountIDR + globalGoldBuyState.serviceFee).toLocaleString()}</div>
                </div>
                
                <div className="mt-5 grid grid-cols-2">
                    <label>Metode Pembayaran</label>
                    <div className="text-right font-medium">{globalGoldBuyState.paymentMethod}</div>
                </div>

                <div className="mt-10">
                    {localState.loading
                        ?
                        <button className="bg-neutral-400 text-white rounded-lg px-5 py-3 font-semibold-500 w-full">
                            <Image src="/img/loading.png"
                                alt="loading"
                                height={16}
                                width={16}
                                className="animate-spin" />
                            <span className="ml-3">Loading ...</span>
                        </button>
                        :                
                        <button className="button-primary rounded-lg px-5 py-3 font-medium w-full"
                            onClick={(e)=>proceed()}>
                            Lanjutkan
                        </button>
                    }
                </div>
            </div>
        } </>
    )
}
