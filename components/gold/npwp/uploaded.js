import React, {useState, useEffect} from "react";
import { useRouter } from 'next/router';

export default function UploadedNPWP(props) {
    const router = useRouter();
    return (
        <>
            {(props.npwp.status ?? null) != "verified" &&
                <div className="mt-7 bg-red-7 p-3 text-white rounded-lg">
                    <div>Your NPWP is under review</div>                                
                </div>
            }

            <div className="mt-7">
                <label className="text-xs">NPWP</label>
                <input type="tel" 
                    className="border-b w-full"
                    placeholder="NPWP Number"
                    maxLength="15"
                    value={props.npwp.npwp}
                    disabled={true} />
            </div>

            <div className="mt-10">
                <button className="button-primary rounded-lg px-5 py-3 font-medium w-full"
                    onClick={()=>router.back()}>
                    Back
                </button>                  
            </div>
        </>
    )
}