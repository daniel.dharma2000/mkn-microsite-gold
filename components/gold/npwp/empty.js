import React, {useState, useEffect} from "react";
import { useRouter } from 'next/router';
import Image from 'next/image';
import Resizer from "react-image-file-resizer";
import * as API from "services/api";
import * as errorCode from "services/error_codes";

export default function EmptyNPWP() {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loadingPage: true, loading: false,            
            npwp:{
                npwp:"",
                photo:"/img/image-placeholder.png"
            },
            error: {
                is:false,
                message:"",
            }
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const showImage = (e) => {
        let objLocalState = localState;
        if(e.target.files[0]) {
            try {
                Resizer.imageFileResizer(
                    e.target.files[0],
                    1000,
                    1000,
                    "JPEG",
                    100,
                    0,
                    (uri) => {
                        objLocalState.npwp.photo = uri;
                        setLocalState({...objLocalState, ...localState});
                    },
                    "base64"
                );         
            } 
            catch (err) {
                objLocalState.npwp.photo = "/img/image-placeholder.png";
                setLocalState({...objLocalState, ...localState});
            }
        }
        else {
            objLocalState.npwp.photo = "/img/image-placeholder.png";
            setLocalState({...objLocalState, ...localState});
        }
    }
    const save = async() => {
        let objLocalState = localState;
        objLocalState.loading = true;
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Post('/account/gold/npwp/save', {
                code: localStorage.getItem("merchantcode"),
                client_code: localStorage.getItem("merchantClientcode"),
                token: localStorage.getItem("logined_token"),
                npwp: localState.npwp.npwp,
                photo: localState.npwp.photo
            });
        }
        catch(error) {
            router.back();
            return true;
        }

        if (response.code == "000") router.reload();
        else {
            let objLocalState = localState;
            objLocalState.loading = true;
            objLocalState.error.is = true;
            objLocalState.error.message = errorCode(response.code);
            setLocalState({...objLocalState, ...localState});
        }
        return true;
    }

    return (
        <>
            <div className="mt-7">
                <label className="text-xs">NPWP</label>
                <input type="tel" 
                    className="border-b w-full"
                    placeholder="NPWP Number"
                    maxLength="15"
                    value={localState.npwp.npwp}
                    onInput={(e)=>setLocalState({...localState, npwp:{...localState.npwp, npwp:e.target.value}})} />
            </div>

            <div className="mt-5">
                <label className="text-xs">NPWP Photo</label>
                <div className="grid grid-cols-2 gap-5">
                    <div>
                        <Image src={localState.npwp.photo}
                            height={300}
                            width={300} />
                    </div>
                    <input type="file" 
                        className="border-b w-full"
                        onChange={(e)=>showImage(e)} />
                </div>                    
            </div>

            <div className="mt-10">
                {localState.loading
                    ?
                    <button className="bg-neutral-400 text-white rounded-lg px-5 py-3 font-medium w-full">
                        <Image src="/img/loading.png"
                            alt="loading"
                            height={16}
                            width={16}
                            className="animate-spin" />
                        <span className="ml-3">Loading ...</span>
                    </button>
                    :
                    <button className="button-primary rounded-lg px-5 py-3 font-medium w-full"
                        onClick={()=>save()}>
                        Save
                    </button>
                }                        
            </div>
        </>
    )
}