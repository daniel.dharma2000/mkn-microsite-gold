import React from "react";
import { useRouter } from 'next/router';

export default function UploadedKTP(props) {
    const router = useRouter();
    return (
        <>
            {(props.ktp.status ?? null) != "verified" &&
                <div className="mt-7 bg-red-7 p-3 text-white rounded-lg">
                    <div>Your KTP is under review</div>                                
                </div>
            }

            <div className="mt-7">
                <label className="text-xs">KTP</label>
                <input type="tel" 
                    className="border-b w-full"
                    placeholder="KTP Number"
                    maxLength="16"
                    value={props.ktp.ktp}
                    disabled={true} />
            </div>

            <div className="mt-10">
                <button className="button-primary rounded-lg px-5 py-3 font-medium w-full"
                    onClick={()=>router.back()}>
                    Back
                </button>                  
            </div>
        </>
    )
}