import React, {useState, useEffect} from "react";
import { useRouter } from 'next/router';
import Image from 'next/image';
import Resizer from "react-image-file-resizer";
import * as API from "services/api";
import * as errorCode from "services/error_codes";

export default function EmptyKTP() {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loadingPage: true, loading: false,            
            ktp:{
                ktp:"",
                photo:"/img/image-placeholder.png",
                selfie:"/img/image-placeholder.png"
            },
            error: {
                is:false,
                message:"",
            }
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const showImage = (e, type) => {
        let objLocalState = localState;
        if(e.target.files[0]) {
            try {
                Resizer.imageFileResizer(
                    e.target.files[0],
                    1000,
                    1000,
                    "JPEG",
                    100,
                    0,
                    (uri) => {
                        if (type == "ktp") objLocalState.ktp.photo = uri;
                        else if (type == "selfie") objLocalState.ktp.selfie = uri;
                        setLocalState({...objLocalState, ...localState});
                    },
                    "base64"
                );         
            } 
            catch (err) {
                if (type == "ktp") objLocalState.ktp.photo = "/img/image-placeholder.png";
                else if (type == "selfie") objLocalState.ktp.selfie = "/img/image-placeholder.png";
                setLocalState({...objLocalState, ...localState});
            }
        }
        else {
            if (type == "ktp") objLocalState.ktp.photo = "/img/image-placeholder.png";
            else if (type == "selfie") objLocalState.ktp.selfie = "/img/image-placeholder.png";
            setLocalState({...objLocalState, ...localState});
        }
    }
    const save = async() => {
        let objLocalState = localState;
        objLocalState.loading = true;
        setLocalState({...objLocalState, ...localState});
        
        try {
            var response = await API.Post('/account/gold/ktp/save', {
                code: localStorage.getItem("merchantcode"),
                client_code: localStorage.getItem("merchantClientcode"),
                token: localStorage.getItem("logined_token"),
                ktp: localState.ktp.ktp,
                photo: localState.ktp.photo,
                selfie: localState.ktp.selfie
            });
        }
        catch(error) {
            router.back();
            return true;
        }

        if (response.code == "000") {
            router.reload();
            return true;
        }
        else {
            objLocalState.loading = false;
            objLocalState.error.is = true;
            objLocalState.error.message = response.message ? response.message : errorCode.getErrorCodes(response.code);;
            setLocalState({...objLocalState, ...localState});
        }
    }

    return (
        <>
            <div className="mt-7">
                <label className="text-xs">KTP</label>
                <input type="tel" 
                    className="border-b w-full"
                    placeholder="KTP Number"
                    maxLength="16"
                    value={localState.ktp.ktp}
                    onInput={(e)=>setLocalState({...localState, ktp:{...localState.ktp, ktp:e.target.value}})} />
            </div>

            <div className="mt-5">
                <label className="text-xs">KTP Photo</label>
                <div className="grid grid-cols-2 gap-5">
                    <div>
                        <Image src={localState.ktp.photo}
                            height={300}
                            width={300} />
                    </div>
                    <input type="file" 
                        className="border-b w-full"
                        onChange={(e)=>showImage(e, "ktp")} />
                </div>                    
            </div>

            <div className="mt-5">
                <label className="text-xs">KTP &amp; Selfie Photo</label>
                <div className="grid grid-cols-2 gap-5">
                    <div>
                        <Image src={localState.ktp.selfie}
                            height={300}
                            width={300} />
                    </div>
                    <input type="file" 
                        className="border-b w-full"
                        onChange={(e)=>showImage(e, "selfie")} />
                </div>                    
            </div>

            {localState.error.is && 
                <div className="mt-5 bg-red-400 px-5 py-3 rounded-lg text-white">
                    {localState.error.message}
                </div>
            }

            <div className="mt-10">
                {localState.loading
                    ?
                    <button className="bg-neutral-400 text-white rounded-lg px-5 py-3 font-medium w-full">
                        <Image src="/img/loading.png"
                            alt="loading"
                            height={16}
                            width={16}
                            className="animate-spin" />
                        <span className="ml-3">Loading ...</span>
                    </button>
                    :
                    <button className="button-primary rounded-lg px-5 py-3 font-medium w-full"
                        onClick={()=>save()}>
                        Save
                    </button>
                }                        
            </div>
        </>
    )
}