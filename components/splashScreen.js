import React from "react";
import Image from 'next/image';

export default function SplashScreen() {
    return (
        <div className="grid place-items-center h-screen">
            <div>                
                <Image src={process.env.NEXT_PUBLIC_LOGO}
                    alt="Nyata"
                    width="150"
                    height="150" />
                <div className="text-center mt-3">
                    <Image src="/img/loading.png"
                        alt="loading"
                        height={16}
                        width={16}
                        className="animate-spin" />                    
                </div>
            </div>
        </div>
    )
}