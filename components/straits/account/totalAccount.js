import React, {useState, useEffect} from "react"
import Image from 'next/image';
import * as API from "services/api";

export default function TotalAccount(props) {
    const initialLocalState = () => {
        return {
            balance:0,
            loadingRefresh: false,
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const setBalance = () => {
        let objLocalState = localState;
        objLocalState.balance = props.balance ?? 0;
        setLocalState({...objLocalState, ...localState});
    }
    const refreshBalance = async() => {
        if(localState.loadingRefresh == false) {
            let objLocalState = localState;
            objLocalState.loadingRefresh = true;
            setLocalState({...objLocalState, ...localState});
        }

        try {
            var response = await API.Get('/account/straits/balance', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`
            );
        }
        catch(error) {
            let objLocalState = localState; 
            objLocalState.loadingRefresh = false;
            setLocalState({...objLocalState, ...localState}); 
            return true;
        }

        let objLocalState = localState; 
        if(response.code == "000") objLocalState.balance = response.data.balance;
        objLocalState.loadingRefresh = false;
        setLocalState({...objLocalState, ...localState}); 
    }
    useEffect(() => {
        setBalance();
    }, []);

    return (
        <div className="rounded bg-primary text-white p-3 drop-shadow">
            <div className="color-white subtitle1">Saldo Kamu</div>
            <div className="mt-1">
                <span>USD</span> 
                <span className="color-white ml-1 mr-2 subtitle1">{parseFloat(localState.balance).toFixed(2)}</span>
                <button onClick={()=>refreshBalance()}>
                    <Image src="/img/refresh.webp" 
                        height="12"
                        width="12"
                        alt="refresh"
                        className={(localState.loadingRefresh ? "animate-spin" : "") } />
                </button>
            </div>
        </div>
    )
}