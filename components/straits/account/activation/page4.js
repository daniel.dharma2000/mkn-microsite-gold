import React, {useState, useEffect} from "react";
import Image from 'next/image';

export default function StraitsAccountActivationPage4(props) {
    const initialLocalState = () => {
        return {
            account: {},
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const saveAccount = (e) => {
        e.preventDefault();
        props.saveAccount(localState.account);
        props.changePage(5);
    }
    useEffect(() => {
        let objLocalState = localState;
        objLocalState.account = props.account;
        setLocalState({...objLocalState, ...localState});
    }, []);

    return (
        <>
            <div className="fixed bg-white w-full z-10">
                <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center">
                    <div className="text-left">
                        <button type="button" 
                            onClick={()=>props.changePage(3)}>
                            <Image src="/img/back.webp" 
                                height="24"
                                width="24"
                                alt="back" />
                        </button>
                    </div>
                    <div className="text-center subtitle1">
                        Aktivasi
                    </div>
                    <div className="text-right">4/8</div>
                </div>
            </div>
            <div className="sm:container mx-auto p-4 custom-pt-70">        
                <div className="subtitle1">
                    Informasi Kontak Darurat
                </div>

                <form onSubmit={(e)=>saveAccount(e)}>
                    <div className="mt-5 border rounded py-1 px-3">
                        <label>Nama</label>
                        <input type="text"
                            placeholder="Nama"
                            className="w-full"
                            value={localState.account.emergency_contact_name ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, emergency_contact_name:e.target.value}})}
                            required={true} />
                    </div>

                    <div className="mt-2 border rounded py-1 px-3">
                        <label>Nomor Handphone</label>
                        <input type="tel"
                            placeholder="Nomor Handphone"
                            className="w-full"
                            value={localState.account.emergency_contact_handphone ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, emergency_contact_handphone:e.target.value}})}
                            required={true} />
                    </div>

                    <div className="mt-2 border rounded py-1 px-3">
                        <label>Hubungan</label>
                        <input type="text"
                            placeholder="Hubungan"
                            className="w-full"
                            value={localState.account.emergency_contact_relation ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, emergency_contact_relation:e.target.value}})}
                            required={true} />
                    </div>

                    <div className="mt-2 border rounded py-1 px-3">
                        <label>Nama Gadis Ibu Kandung</label>
                        <input type="text"
                            placeholder="Nama Gadis Ibu Kandung"
                            className="w-full"
                            value={localState.account.mother_name ?? ""}
                            onInput={(e) => setLocalState({...localState, account:{...localState.account, mother_name:e.target.value}})}
                            required={true} />
                    </div>

                    <div className="mt-5 grid grid-cols-2 gap-3">
                        <button className="button-secondary w-full"
                            onClick={()=>props.changePage(3)}
                            type="button">
                            Kembali
                        </button>
                        <button className="button-primary w-full"
                            type="submit">
                            Selanjutnya
                        </button>
                    </div>
                </form>
            </div>
        </>
    )
}
