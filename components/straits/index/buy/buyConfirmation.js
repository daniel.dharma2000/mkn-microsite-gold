import React, {useState, useEffect} from "react";
import Link from 'next/link';
import Image from 'next/image';
import * as API from "services/api";
import LoadingPage from "components/loadingPage";
import { useRouter } from 'next/router';

export default function BuyConfirmation(props) {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            account: {},
            symbol:{},
            kurs:0,
            loadingAccount:true,
            loadingSymbol:true,
            loading: false,
            isError:false,
            error_message:""
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getAccount = async() => {
        let objLocalState = localState;
        objLocalState.loadingAccount = true;
        objLocalState.account = {};
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/account/straits', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            router.push('/logined/dashboard');
            return true;
        }

        if(response.code == "000") {
            objLocalState.account = response.data.straits_account;
            objLocalState.loadingAccount = false;
            setLocalState({...objLocalState, ...localState});
        }
        else router.push('/logined/dashboard');
        return true;
    }
    const getSymbol = async() => {
        let objLocalState = localState;
        objLocalState.loadingSymbol = true;
        objLocalState.symbol = {};
        objLocalState.kurs = 0;
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/account/straits/symbol/realtime', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}&symbol=${router.query.index}`);
        }
        catch(error) {
            router.back();
            return true;
        }

        if(response.code == "000") {
            objLocalState.loadingSymbol = false;
            objLocalState.symbol = response.data.symbol;
            objLocalState.kurs = response.data.kurs;
            setLocalState({...objLocalState, ...localState});
        }
        else router.back();
        return true;
    }
    const proceed = async() => {
        let objLocalState = localState;
        objLocalState.loading = true;
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Post('/account/straits/symbol/buy', {
                code: localStorage.getItem("merchantcode"),
                client_code: localStorage.getItem("merchantClientcode"),
                token: localStorage.getItem("logined_token"),
                symbol: router.query.index,
                volume: props.unit
            });
        }
        catch(error) {
            router.back();
            return true;
        }

        if(response.code == "000") {
            if(response.data.success == "true") router.push('/logined/index/transaction/success');
            else router.push('/logined/index/transaction/failed');
        }
        else router.back();
        return true;
    }
    useEffect(() => {
        if(!router.isReady) return;
        getAccount();
        getSymbol();
    }, [router.isReady]);

    return (
        <>
            {localState.loadingSymbol || localState.loadingAccount
                ?
                <LoadingPage />
                :
                <>
                    <div className="fixed bg-white w-full z-10">
                        <div className="sm:container p-4 mx-auto grid grid-cols-3 items-center">
                            <div className="text-left">
                                <button type="button" 
                                    onClick={() => router.back()}>
                                    <Image src="/img/back.webp" 
                                        height="24"
                                        width="24"
                                        alt="back" />
                                </button>
                            </div>
                            <div className="text-center subtitle1">
                                Konfirmasi
                            </div>
                        </div>
                    </div>

                    <div className="sm:container mx-auto p-4 custom-pt-70">                        
                        <div className="grid grid-cols-2">
                            <div className="flex">
                                <div>
                                    <Image src={localState.symbol.image} 
                                        height="24"
                                        width="24"
                                        alt={localState.symbol.name}
                                        className="rounded-full" />
                                </div>
                                <div className="ml-2">
                                    <div className="font-semibold">{localState.symbol.symbol}</div>
                                    <div>{localState.symbol.name}</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="sm:container mx-auto p-4 bg-gray-2">
                        <div className="grid grid-cols-2">
                            <div className="self-center">Jumlah Unit</div>
                            <div className="text-right">{props.unit}</div>
                        </div>

                        <div className="mt-2 grid grid-cols-2">
                            <div className="self-center">Perkiraan Harga Beli</div>
                            <div className="text-right">{localState.symbol.currency} {localState.symbol.price}</div>
                        </div>

                        <div className="mt-2 grid grid-cols-2">
                            <div className="self-center">Saldo</div>
                            <div className="text-right">USD {localState.account.balance}</div>
                        </div>

                        {localState.symbol.currency != "USD" &&
                            <div className="mt-2 grid grid-cols-2">
                                <label className="self-center">1 USD/{localState.symbol.currency}</label>
                                <div className="text-right">USD {localState.kurs}</div>
                            </div>                
                        }

                        <hr className="mt-2" />

                        <div className="mt-2 grid grid-cols-2">
                            <label className="self-center">Perkiraan Nilai Pembelian</label>
                            <div className="text-right font-semibold">USD 
                                {(parseFloat(localState.symbol.price) * parseFloat(props.unit) / parseFloat(localState.kurs)).toFixed(2)}</div>
                        </div>
                    </div>

                    <div className="sm:container mx-auto p-4 mt-5">
                        <div className="bg-gray-10-75 rounded p-4">
                            <div className="subtitle2 color-white">Lainnya</div>
                            <div className="text-white">
                                Pembelian Anda akan dieksekusi secara Market Order, yaitu pada harga terbaik yang mungkin didapat.<br />
                                Harga Beli mungkin berubah setelah pesanan dilakukan.<br/>
                                Pesanan juga dapat gagal apabila saldo tidak mencukupi.
                            </div>
                        </div>

                        <div className="mt-5">
                            {localState.loading
                                ?
                                <button className="button-primary w-full"
                                    disabled="disabled">
                                    <Image src="/img/loading.png"
                                        alt="loading"
                                        height={16}
                                        width={16}
                                        className="animate-spin" />
                                    <span className="ml-3">Loading ...</span>
                                </button>
                                :                
                                <button className="button-primary w-full"
                                    onClick={(e)=>proceed()}>
                                    Selesaikan
                                </button>
                            }
                        </div>
                    </div>
                </>
            }
        </>
    )
}
