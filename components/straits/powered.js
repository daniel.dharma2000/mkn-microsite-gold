import React from "react";
import Image from 'next/image'

export default function Powered() {
    return (
        <div className="sm:container mx-auto p-4">
            <div className="float-left pr-2">
                <Image src="/img/index.png" 
                    height="24"
                    width="24"
                    alt="Kayya" />
            </div>
            <div className="color-gray text-justify"
                style={{fontSize:8}}>
                Kayya difasilitasi oleh PT Straits Futures Indonesia, Pialang Berjangka yang diawasi oleh Bappebti berdasarkan izin nomor 43/BAPPEBTI/SI/09/2015. Transaksi Anda akan diregistrasikan ke Jakarta Futures Exchange dan dijamin penyelesaiannya oleh PT Kliring Berjangka Indonesia (Persero)
            </div>     
        </div>
    )
}