import React, {useState, useEffect} from "react";
import Image from 'next/image';
import { useRouter } from 'next/router';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import LoadingPage from "components/loadingPage";
import * as API from "services/api";

export default function WithdrawInput(props) {
    const router = useRouter();
    const initialLocalState = () => {
        return {
            loadingPage: true, 
            loadingProceed: false,
            account: {},
            value: "0"
        }
    };
    const [localState, setLocalState] = useState(initialLocalState());
    const getAccount = async() => {
        let objLocalState = localState;
        objLocalState.loadingPage = true;
        objLocalState.account = {};
        setLocalState({...objLocalState, ...localState});

        try {
            var response = await API.Get('/account/straits', 
                `code=${localStorage.getItem("merchantcode")}&client_code=${localStorage.getItem("merchantClientcode")}&token=${localStorage.getItem("logined_token")}`);
        }
        catch(error) {
            router.push('/logined/dashboard');
            return true;
        }

        if(response.code == "000") {
            objLocalState.account = response.data.straits_account;
            objLocalState.loadingPage = false;
            setLocalState({...objLocalState, ...localState});
        }
        else router.push('/logined/dashboard');
        return true;
    }
    const proceed = (e) => {
        e.preventDefault();

        if(localState.value == "0") {
            toast.error("Nilai yang anda masukan harus lebih dari 0 USD");
            return true;
        }

        props.changePage("withdrawConfirmation", localState.value);
        return true;
    }
    useEffect(() => {
        getAccount();
    }, []);

    return (
        <>
            {localState.loadingPage
                ?
                <LoadingPage />
                :
                <>
                    <ToastContainer />
                    <div className="fixed bg-white w-full z-10">
                        <div className="sm:container p-4 mx-auto">                       
                            <div className="grid grid-cols-3 items-center">  
                                <div className="text-left">
                                    <button type="button" 
                                        onClick={() => router.back()}>
                                        <Image src="/img/back.webp" 
                                            height="24"
                                            width="24"
                                            alt="back" />
                                    </button>
                                </div>
                                <div className="text-center subtitle1">
                                    Withdraw
                                </div>
                            </div>
                        </div>
                    </div>
                    <form onSubmit={(e)=>proceed(e)}>
                        <div className="sm:container mx-auto p-4 custom-pt-70"
                            style={{paddingBottom:80}}>                        
                            <div className="mt-10 text-center">                        
                                <div className="font-semibold">Saldo USD Tersedia USD {localState.account.balance}</div>
                            </div>
                            <div className="mt-20">
                                <div className="text-center">Nilai USD</div>
                                <div className="font-semibold text-center w-full text-4xl text-blue-600">
                                    <input type="number"
                                        className="text-center w-full"
                                        autoFocus={true}
                                        min="0"
                                        max={localState.account.balance}
                                        step={0.01}
                                        placeholder="Nilai USD"
                                        onInput={(e) => setLocalState({...localState, value:e.target.value})}
                                        value={localState.value} />
                                </div>                           
                            </div>
                        </div>
                        <div className="fixed bottom-0 w-full">
                            <div className="sm:container mx-auto p-4">
                                {localState.loadingProceed
                                    ?
                                    <button className="button-primary w-full"
                                        disabled="disabled"
                                        type="submit">
                                        <Image src="/img/loading.png"
                                            alt="loading"
                                            height={16}
                                            width={16}
                                            className="animate-spin" />
                                        <span className="ml-3">Loading ...</span>
                                    </button>
                                    :                
                                    <button className="button-primary w-full"
                                        type="submit">
                                        Lanjutkan
                                    </button>
                                }
                            </div>
                        </div>
                    </form>
                </>
            }
        </>
    )
}
