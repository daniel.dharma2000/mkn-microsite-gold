import React from "react"
import { useRouter } from 'next/router'

export default function PendingBank(props) {
    const router = useRouter();
    return (
        <>
            <div className="mt-10 bg-red-4 px-3 py-1 rounded-lg text-sm">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit,
            </div>

            <div className="mt-10">
                <label className="text-xs">Bank</label>
                <input type="text" 
                    className="border-b w-full"
                    disabled="true"
                    value={props.customerBank.bank.name} />
            </div>

            <div className="mt-5">
                <label className="text-xs">Account Number</label>
                <input type="tel" 
                    className="border-b w-full"
                    disabled="true"
                    value={props.customerBank.account_number} />
            </div>

            <div className="mt-10">
                <button className="button-primary rounded-lg px-5 py-3 font-medium w-full"
                    onClick={()=>router.back()}>
                    Back
                </button>
            </div>
        </>
    )
}