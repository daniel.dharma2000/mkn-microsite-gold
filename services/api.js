import axios from 'axios';

const instance = axios.create({timeout: 10000});
let cancelToken;

function check_response(response) {
    if(response.data.code == '001') {
        localStorage.clear();
        location.href = '/';
    }
    return true;
}

export async function Get(url, data) {
    if (typeof cancelToken != typeof undefined) { cancelToken.cancel("Operation canceled due to new request."); }  
    cancelToken = axios.CancelToken.source();

    try {
        var res = await instance.get(process.env.NEXT_PUBLIC_APIURL+url+'?'+data); 
    } 
    catch (e) { throw new Error("Failed to connect to server"); }
    return res.data; 
}

export async function Post(url, data) {
    if (typeof cancelToken != typeof undefined) { cancelToken.cancel("Operation canceled due to new request."); }  
    cancelToken = axios.CancelToken.source();

    try {
        var res = await instance.post(process.env.NEXT_PUBLIC_APIURL+url, data); 
    } 
    catch (e) { throw new Error("Failed to connect to server"); }
    return res.data; 
}