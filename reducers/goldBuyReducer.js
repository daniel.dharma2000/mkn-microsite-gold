export const SET_DATA = 'SET_DATA';
export const SET_PAGE = 'SET_PAGE';

export const  globalInitialState = {
    paymentMethod: "",
    amountGold: 0,
    amountIDR: 0,
    serviceFee: 0,
    price: 0,

    page: "inquiry"
};

const GoldBuyReducer = (globalGoldBuyState = globalInitialState, action) => {
    switch (action.type) {
        case SET_DATA:{
            let {
                paymentMethod = (globalGoldBuyState.paymentMethod ?? globalInitialState.paymentMethod),
                amountGold = (globalGoldBuyState.amountGold ?? globalInitialState.amountGold),
                amountIDR = (globalGoldBuyState.amountIDR ?? globalInitialState.amountIDR),
                serviceFee = (globalGoldBuyState.serviceFee ?? globalInitialState.serviceFee),
                price = (globalGoldBuyState.price ?? globalInitialState.price),
            } = action;

            return {
                ...globalGoldBuyState,
                paymentMethod, amountGold, amountIDR, serviceFee, price
            };
        }

        case SET_PAGE:{
            let {
                page = globalGoldBuyState.page
            } = action;

            return {
                ...globalGoldBuyState,
                page
            };
        }
        
        default:
            return {...globalGoldBuyState, ...globalInitialState};
    }
};

export default GoldBuyReducer;